package com.kotlin.reset

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
open class RestKotlinApplication

fun main(args: Array<String>) {
//	runApplication<RestKotlinApplication>(*args) {
//		setBannerMode(Banner.Mode.OFF)
//	}
	 //SpringApplication.run(RestKotlinApplication::class.java, *args)
	runApplication<RestKotlinApplication>(*args)
}
