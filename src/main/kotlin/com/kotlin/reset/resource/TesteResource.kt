package com.kotlin.reset.resource

import com.kotlin.reset.model.Person
import com.kotlin.reset.repository.PersonRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController


@RestController
@RequestMapping("/teste")
class TesteResource    {

	@Autowired
	lateinit var repository: PersonRepository
//
//	@GetMapping("/{id}")
//	fun findById(@PathVariable id: Int): Person? = repository.findById(id)
//
//	@GetMapping
//	fun findAll(): List<Person> = repository.findAll()
//
//	@PostMapping
//	fun add(@RequestBody person: Person): Person = repository.save(person)
//
//	@PutMapping
//	fun update(@RequestBody person: Person): Person = repository.update(person)
//
//	@DeleteMapping("/{id}")
//	fun remove(@PathVariable id: Int): Boolean = repository.removeById(id)
}