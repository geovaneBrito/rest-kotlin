package com.kotlin.reset.model

enum class Gender {
    MALE, FEMALE
}