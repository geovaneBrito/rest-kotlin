package com.kotlin.reset.model

import com.kotlin.reset.model.Gender

data class Person(var id: Int?, var name: String, var age: Int, var gender: Gender)
